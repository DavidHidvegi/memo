import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

import 'entrymodels/entrymodels.dart';
import 'myroute.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocumentDirectory =
      await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(PulseAdapter());
  Hive.registerAdapter(ExerciseAdapter());
  Hive.registerAdapter(BloodPressureAdapter());
  Hive.registerAdapter(SleepQualityGroningenAdapter());
  runApp(
    MaterialApp(
        theme: ThemeData(
          scaffoldBackgroundColor: Color(0xFFF9F9F9),
          backgroundColor: Color(0xFFF9F9F9),
          primarySwatch: Colors.grey,
        ),
        home: HomeRoute()),
  );
}
