import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'mytext.dart';
import 'mytextstyle.dart';

class EntryElement {
  static Container dateTimeInput(titleText, Function onEventCallback,
      {DateTime initialDateTime}) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
            margin: EdgeInsets.only(top: 3, bottom: 6),
            child: MyText.heading2(titleText),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 2),
              child: DateTimeInput(onEventCallback,
                  initialDateTime: initialDateTime)),
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  static Container textInput(titleText,
      {String suffixText,
      String hintText,
      Function onEventCallback,
      String initialValue}) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
            margin: EdgeInsets.only(top: 3, bottom: 6),
            child: MyText.heading2(titleText),
          ),
          Container(
              child: TextFormField(
            initialValue:
                (initialValue == null) ? null : initialValue.toString(),
            onFieldSubmitted: (text) {
              onEventCallback(text);
            },
            style: MyTextStyle.paragraph(),
            decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.all(0.0),
                hintStyle: MyTextStyle.hint(),
                suffixStyle: MyTextStyle.paragraph(),
                border: InputBorder.none,
                hintText: hintText,
                suffixText: suffixText),
            keyboardType: TextInputType.text,
          ))
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  static Container numberInput(titleText,
      {String suffixText,
      String hintText,
      Function onEventCallback,
      int initialValue}) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
            margin: EdgeInsets.only(top: 3, bottom: 6),
            child: MyText.heading2(titleText),
          ),
          Container(
              child: TextFormField(
            initialValue:
                (initialValue == null) ? null : initialValue.toString(),
            onFieldSubmitted: (text) {
              onEventCallback(int.parse(text));
            },
            style: MyTextStyle.paragraph(),
            decoration: new InputDecoration(
                isDense: true,
                contentPadding: EdgeInsets.all(0.0),
                hintStyle: MyTextStyle.hint(),
                suffixStyle: MyTextStyle.paragraph(),
                border: InputBorder.none,
                hintText: hintText,
                suffixText: suffixText),
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              WhitelistingTextInputFormatter.digitsOnly
            ],
          ))
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  static Container titledMultipleChoiseInput(titleText, List<String> choiseList,
      {onEventCallback, initialValue, oneAnswerPerLine = true}) {
    final List<String> reportList = choiseList;
    if( oneAnswerPerLine)
    return Container(
      child: Column(
        children: <Widget>[
          MyText.heading2(titleText),
          Container(
            height: 5,
          ),
          MultiSelectChip(
            reportList,
            onEventCallback,
            initialValue: initialValue,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
      padding: EdgeInsets.only(top: 3),
    );
    else
    return Container(
      child: Wrap(
        children: <Widget>[
          MyText.heading2(titleText),
          Container(
            height: 5,
          ),
          MultiSelectChip(
            reportList,
            onEventCallback,
            initialValue: initialValue,
          )
        ],
      ),
      padding: EdgeInsets.only(top: 3),
    );
  }
}

class DateTimeInput extends StatefulWidget {
  final Function onEventCallback;
  final DateTime initialDateTime;
  DateTimeInput(this.onEventCallback, {this.initialDateTime});

  @override
  _DateTimeInputState createState() =>
      _DateTimeInputState(initialDateTime: initialDateTime);
}

class _DateTimeInputState extends State<DateTimeInput> {
  DateTime initialDateTime;
  _DateTimeInputState({this.initialDateTime});
  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime =
      TimeOfDay(hour: DateTime.now().hour, minute: DateTime.now().minute);

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null) {
      selectedDate = picked;
      _selectTime(context);
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
      builder: (BuildContext context, Widget child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedDate = DateTime(selectedDate.year, selectedDate.month,
            selectedDate.day, picked.hour, picked.minute);
        widget.onEventCallback(selectedDate);
      });
  }

  @override
  Widget build(BuildContext context) {
    if (this.initialDateTime != null) {
      selectedDate = this.initialDateTime;
      selectedTime =
          TimeOfDay(hour: selectedDate.hour, minute: selectedDate.minute);
      this.initialDateTime = null;
    }
    String dateFormat =
        new DateFormat("yyyy MM dd - HH:mm").format(selectedDate);
    widget.onEventCallback(selectedDate);

    return Container(
      height: 20,
      child: FlatButton(
          padding: EdgeInsets.all(0),
          onPressed: () => _selectDate(context),
          child: Container(
              child: MyText.paragraph(dateFormat),
              constraints: BoxConstraints.expand())),
    );
  }
}

class MultiSelectChip extends StatefulWidget {
  final List<String> reportList;
  final onEventCallback;
  final String initialValue;
  MultiSelectChip(this.reportList, this.onEventCallback, {this.initialValue});
  @override
  _MultiSelectChipState createState() =>
      _MultiSelectChipState(onEventCallback, initialValue: initialValue);
}

class _MultiSelectChipState extends State<MultiSelectChip> {
  Function onEventCallback;
  String initialValue;
  _MultiSelectChipState(this.onEventCallback, {this.initialValue});
  String selectedChoice = "";
  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        height: 30,
        margin: EdgeInsets.only(top: 7),
        padding: EdgeInsets.all(0.0),
        child: ChoiceChip(
          label: Text(item, style: MyTextStyle.tag()),
          selectedColor: Colors.green[700],
          selected: selectedChoice == item,
          backgroundColor: Color(0xFF828282),
          padding: EdgeInsets.only(top: 3),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8))),
          onSelected: (selected) {
            setState(() {
              selectedChoice = item;
              widget.onEventCallback(item);
            });
          },
        ),
      ));
    });
    return choices;
  }

  @override
  Widget build(BuildContext context) {
    if (initialValue != null) {
      selectedChoice = initialValue;
      initialValue = null;
    }
    return Column(
      children: _buildChoiceList(),
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
    );
  }
}
