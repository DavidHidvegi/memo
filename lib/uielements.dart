import 'package:flutter/material.dart';
import 'package:memo/mytext.dart';

import 'myimage.dart';

class UIElements {
  static IconButton backIconButton(onPressed) {
    return IconButton(
        iconSize: 30,
        padding: EdgeInsets.all(6),
        icon: MyImage.arrowBack(),
        onPressed: onPressed);
  }

  static IconButton crossIconButton(onPressed) {
    return IconButton(
        iconSize: 30,
        padding: EdgeInsets.all(6),
        icon: MyImage.cross(),
        onPressed: onPressed);
  }

  static IconButton tickIconButton(onPressed) {
    return IconButton(
        iconSize: 30,
        padding: EdgeInsets.all(6),
        icon: MyImage.tick(),
        onPressed: onPressed);
  }

  static FlatButton titledButton(onPressed, title, description) {
    return FlatButton(
      onPressed: onPressed,
      child: titledDescription(title, description),
      padding: EdgeInsets.all(0),
    );
  }

  static Widget titledDescription(title, description) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        children: <Widget>[
          Container(
            height: 20,
            margin: EdgeInsets.only(top: 3, bottom: 6),
            child: MyText.heading2(title),
          ),
          Container(
              margin: EdgeInsets.only(bottom: 2),
              child: MyText.paragraph(description)),
        ],
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
      ),
    );
  }

  static FloatingActionButton floatingActionButton(onPressed) {
    return FloatingActionButton(
      onPressed: onPressed,
      child: Container(
        child: MyImage.tick(),
        height: 30,
      ),
      backgroundColor: Color(0xFFEFEFEF),
      elevation: 0.0,
    );
  }
}
