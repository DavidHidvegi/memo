import 'package:flutter/material.dart';

import './mytextstyle.dart';

class MyText {
  static Text heading1(text) {
    return Text(
      text,
      style: MyTextStyle.heading1(),
    );
  }

  static Text heading2(text) {
    return Text(
      text,
      style: MyTextStyle.heading2(),
    );
  }

  static Text paragraph(text) {
    return Text(
      text,
      style: MyTextStyle.paragraph(),
    );
  }

  static Text tag(text) {
    return Text(
      text,
      style: MyTextStyle.tag(),
    );
  }
}