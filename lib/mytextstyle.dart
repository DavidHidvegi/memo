import 'package:flutter/material.dart';

class MyTextStyle {
  static TextStyle heading1() {
    return TextStyle(
        fontFamily: 'Poppins',
        height: 1,
        fontSize: 36,
        fontWeight: FontWeight.bold,
        color: Color(0xFF333333));
  }

  static TextStyle heading2() {
    return TextStyle(
      fontFamily: 'Poppins',
      height: 1,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      color: Color(0xFF4F4F4F),
    );
  }

  static TextStyle paragraph() {
    return TextStyle(
      fontFamily: 'Poppins',
      height: 1,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      color: Color(0xFF828282),
    );
  }

  static TextStyle hint() {
    return TextStyle(
      fontFamily: 'Poppins',
      height: 1,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.italic,
      color: Color(0xFF828282),
    );
  }

  static TextStyle tag() {
    return TextStyle(
      fontFamily: 'Poppins',
      height: 1,
      fontSize: 16,
      fontWeight: FontWeight.w500,
      color: Color(0xFFF2F2F2),
    );
  }
}
