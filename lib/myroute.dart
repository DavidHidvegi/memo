import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:memo/bacterialprogramming/ability.dart';
import 'package:memo/bacterialprogramming/bacterialprogramming.dart';
import 'package:memo/bacterialprogramming/tree.dart';
import 'package:memo/entrymodels/entrymodels.dart';

import 'mytext.dart';
import 'uielements.dart';

class RouteFrame {
  Widget titleWidget;
  IconButton topLeft;
  Widget floatingActionButton;
  List<Widget> widgets;
  RouteFrame(Widget titleWidget, IconButton topLeft,
      FloatingActionButton floatingActionButton, List<Widget> widgets) {
    this.titleWidget = titleWidget;
    this.topLeft = topLeft;
    this.floatingActionButton = floatingActionButton;
    this.widgets = widgets;
  }

  Scaffold scaffold() {
    List<Widget> finalWidgets = List<Widget>();

    if (topLeft != null)
      finalWidgets.add(Container(
        child: topLeft,
        height: 80,
        padding: EdgeInsets.only(left: 20, top: 20),
        alignment: Alignment.topLeft,
      ));
    else
      finalWidgets.add(Container(
        height: 80,
      ));

    finalWidgets.add(Container(
      padding: EdgeInsets.only(left: 30, bottom: 50, top: 40, right: 30),
      child: titleWidget,
    ));

    for (var i = 0; i < widgets.length; i++) {
      finalWidgets.add(Container(
        padding: EdgeInsets.only(left: 30, bottom: 30, right: 30),
        child: widgets[i],
      ));
    }

    finalWidgets.add(Container(
      height: 120,
    ));

    return Scaffold(
        body: SafeArea(
          child: CustomScrollView(slivers: <Widget>[
            new SliverList(delegate: new SliverChildListDelegate(finalWidgets))
          ]),
        ),
        floatingActionButton: floatingActionButton);
  }
}

class HomeRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RouteFrame(MyText.heading1("Memo"), null, null, [
      UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ChooseEntryTypeRoute()),
        );
      }, "Új Adat", 'Jegyezz fel új adatot.'),
      UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SearchRoute()),
        );
      }, "Kerersés", 'Keress a feljegyzett adataid között.'),
      UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ChooseDiscoveryRoute()),
        );
      }, "Felfedezés", 'Fedezd fel önmagad az adataid alapján.'),
      UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SettingsRoute()),
        );
      }, "Beállítások", 'Konfiguráld az alkalmazást az igényeid szerint.'),
    ]).scaffold();
  }
}

class ChooseEntryTypeRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<EntryModel> models = EntryModels.models;
    List<Widget> widgets = List<Widget>();
    for (var i =0; i < models.length; i++){
      widgets.add(UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => EntryRoute(EntryModels.models[i])),
        );
      }, models[i].titleText, models[i].callToActionText));
    }
    return RouteFrame(MyText.heading1("Adattípus"),
            UIElements.backIconButton(() {
      Navigator.pop(context);
    }), null, widgets)
        .scaffold();
  }
}

class SearchRoute extends StatefulWidget {
  @override
  _SearchRoute createState() {
    return _SearchRoute();
  }
}

class _SearchRoute extends State<SearchRoute> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: openBoxes(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError)
            return Text(snapshot.error.toString());
          else {
            return content();
          }
        } else
          return Scaffold();
      },
    );
  }

  Future<Box<dynamic>> openBoxes() async {
    List<EntryModel> models = EntryModels.models;
    for (var model in models) {
      await Hive.openBox(model.boxName);
    }
    return await Hive.openBox(models[0].boxName);
  }

  Widget content() {
    List<Widget> widgets = List<Widget>();
    List<EntryModel> models = EntryModels.models;
    List entries = List();
    for (var model in models) {
      Box box = Hive.box(model.boxName);
      for (int i = 0; i < box.length; i++) {
        entries.add([box.getAt(i), i]);
      }
    }
    entries.sort((a, b) => b[0].dateTime.compareTo(a[0].dateTime));
    for (int i = 0; i < entries.length; i++) {
      widgets.add(UIElements.titledButton(() {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => EntryRoute(
                    entries[i][0],
                    editEntryIndex: entries[i][1],
                  )),
        );
      }, entries[i][0].titleText, entries[i][0].shortDescription()));
    }
    return RouteFrame(MyText.heading1("Keresés"), UIElements.backIconButton(() {
      Navigator.pop(context);
    }), null, widgets)
        .scaffold();
  }

  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }
}

class ChooseDiscoveryRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Function fitnessFunction = (Tree tree) {
      int diff = tree.eval() - 23;
      if (diff < 0) diff = -1 * diff;
      diff = diff + tree.nodeCount ~/ 10;
      return diff;
    };
    Ability a1 = Ability(() {
      return 1;
    }, int);
    Ability a2 = Ability(() {
      Random r = Random();
      return r.nextInt(10);
    }, int);
    Ability sum = Ability((a, b) {
      return a + b;
    }, int, argTypesList: [int, int]);
    Ability mul = Ability((a, b) {
      return a * b;
    }, int, argTypesList: [int, int]);
    List<Widget> widgets = List<Widget>();
    for (var j = 0; j < 50; j++) {
      BacterialProgramming bp = BacterialProgramming(
          [a1, a2, sum, mul], fitnessFunction,
          infectionCount: 10, limitNodeCount: 5);
      for (var i = 0; i < 20; i++) {
        widgets.add(UIElements.titledDescription(
            "Bakteriális Fa",
            'A legjobb Fa eredménye: ${bp.bestTree().eval()}\n' +
                'A legjobb fa elemeinek száma: ${bp.bestTree().nodeCount}\n' +
                'A legrosszabb fa elemeinek száma: ${bp.population[9].nodeCount}\n'));
        bp.evolveUntil(bp.genCount + 1, 0);
      }
    }

    return RouteFrame(MyText.heading1("Felfedezés"),
            UIElements.backIconButton(() {
      Navigator.pop(context);
    }), null, widgets)
        .scaffold();
  }
}

class SettingsRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RouteFrame(MyText.heading1("Beállítások"),
        UIElements.backIconButton(() {
      Navigator.pop(context);
    }), null, [
      UIElements.titledDescription(
          'Adatok exportálása', 'Az összes adat exportálása.'),
      UIElements.titledDescription(
          'Adatok importálása', 'Adatok importálása file-ból')
    ]).scaffold();
  }
}

class EntryRoute extends StatefulWidget {
  final EntryModel entryModel;
  final int editEntryIndex;
  EntryRoute(this.entryModel, {this.editEntryIndex});

  @override
  _EntryRoute createState() {
    return _EntryRoute();
  }
}

class _EntryRoute extends State<EntryRoute> {
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Hive.openBox(widget.entryModel.boxName),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError)
            return Text(snapshot.error.toString());
          else {
            final box = Hive.box(widget.entryModel.boxName);
            if (widget.editEntryIndex == null) {
              return content(widget.entryModel, box);
            } else {
              return content(box.getAt(widget.editEntryIndex), box,
                  editEntryIndex: widget.editEntryIndex);
            }
          }
        } else
          return Scaffold();
      },
    );
  }

  Widget content(EntryModel entryModel, Box box, {editEntryIndex}) {
    return RouteFrame(MyText.heading1(entryModel.titleText),
            UIElements.crossIconButton(() {
      Navigator.pop(context);
    }), UIElements.floatingActionButton(() {
      if (entryModel.readyToSave()) {
        if (editEntryIndex == null)
          box.add(entryModel);
        else
          box.putAt(editEntryIndex, entryModel);
        Navigator.pop(context);
      }
    }), entryModel.widgets)
        .scaffold();
  }

  @override
  void dispose() {
    Hive.close();
    super.dispose();
  }
}
