import 'package:flutter/material.dart';

class MyImage{
  static Image arrowBack() {
    return Image.asset(
      'assets/imgs/arrow_back_3x.png',
      color: Colors.black87,
    );
  }

  static Image cross() {
    return Image.asset(
      'assets/imgs/cross_3x.png',
      color: Colors.black87,
    );
  }

  static Image tick() {
    return Image.asset(
      'assets/imgs/tick_3x.png',
      color: Colors.black87,
    );
  }
}