// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'entrymodels.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PulseAdapter extends TypeAdapter<Pulse> {
  @override
  final typeId = 0;

  @override
  Pulse read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Pulse()
      ..dateTime = fields[0] as DateTime
      ..pulse = fields[1] as int;
  }

  @override
  void write(BinaryWriter writer, Pulse obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.dateTime)
      ..writeByte(1)
      ..write(obj.pulse);
  }
}

class ExerciseAdapter extends TypeAdapter<Exercise> {
  @override
  final typeId = 1;

  @override
  Exercise read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Exercise()
      ..dateTime = fields[0] as DateTime
      ..exerciseName = fields[1] as String
      ..exerciseLegth = fields[2] as int
      ..exerciseDifficulty = fields[3] as int;
  }

  @override
  void write(BinaryWriter writer, Exercise obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.dateTime)
      ..writeByte(1)
      ..write(obj.exerciseName)
      ..writeByte(2)
      ..write(obj.exerciseLegth)
      ..writeByte(3)
      ..write(obj.exerciseDifficulty);
  }
}

class BloodPressureAdapter extends TypeAdapter<BloodPressure> {
  @override
  final typeId = 2;

  @override
  BloodPressure read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return BloodPressure()
      ..dateTime = fields[0] as DateTime
      ..systolicPressure = fields[1] as int
      ..diastolicPressure = fields[2] as int;
  }

  @override
  void write(BinaryWriter writer, BloodPressure obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.dateTime)
      ..writeByte(1)
      ..write(obj.systolicPressure)
      ..writeByte(2)
      ..write(obj.diastolicPressure);
  }
}

class SleepQualityGroningenAdapter extends TypeAdapter<SleepQualityGroningen> {
  @override
  final typeId = 3;

  @override
  SleepQualityGroningen read(BinaryReader reader) {
    var numOfFields = reader.readByte();
    var fields = <int, dynamic>{
      for (var i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SleepQualityGroningen()
      ..dateTime = fields[0] as DateTime
      ..answerList = (fields[1] as List)?.cast<dynamic>();
  }

  @override
  void write(BinaryWriter writer, SleepQualityGroningen obj) {
    writer
      ..writeByte(2)
      ..writeByte(0)
      ..write(obj.dateTime)
      ..writeByte(1)
      ..write(obj.answerList);
  }
}
