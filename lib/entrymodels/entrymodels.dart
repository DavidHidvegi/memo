import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';

import '../entryelement.dart';

part 'entrymodels.g.dart';

class EntryModels {
  static List<EntryModel> get models {
    return [
      SleepQualityGroningen(),
      Pulse(),
      Exercise(),
      BloodPressure(),
    ];
  }
}

abstract class EntryModel {
  DateTime dateTime;
  String boxName;
  String titleText;
  String callToActionText;
  List<Widget> widgets;
  bool readyToSave();
  String shortDescription();
}

@HiveType(typeId: 0)
class Pulse extends EntryModel {
  @HiveField(0)
  DateTime dateTime;

  @HiveField(1)
  int pulse;

  bool readyToSave() {
    return (this.pulse != null && this.dateTime != null);
  }

  @override
  String shortDescription() {
    return '${DateFormat("yyyy MM dd - HH:mm").format(dateTime)} Pulzus szám: $pulse';
  }

  String boxName = 'pulse';
  String titleText = 'Pulzus Szám';
  String callToActionText = 'Kövesd a pillanatnyi pulzusod.';
  List<Widget> get widgets {
    return [
      EntryElement.dateTimeInput("Dátum és Idő", (dateTime) {
        this.dateTime = dateTime;
      }, initialDateTime: this.dateTime),
      EntryElement.numberInput('Pulzus Szám',
          hintText: 'Add meg a pulzus számot',
          suffixText: '[ütés/perc]', onEventCallback: (number) {
        this.pulse = number;
      }, initialValue: this.pulse),
    ];
  }
}

@HiveType(typeId: 1)
class Exercise extends EntryModel {
  @HiveField(0)
  DateTime dateTime;

  @HiveField(1)
  String exerciseName;

  @HiveField(2)
  int exerciseLegth;

  @HiveField(3)
  int exerciseDifficulty;

  bool readyToSave() {
    return (this.dateTime != null &&
        this.exerciseLegth != null &&
        this.exerciseDifficulty != null);
  }

  @override
  String shortDescription() {
    String activityType =
        (exerciseName == null || exerciseName.isEmpty) ? '' : '$exerciseName: ';
    return '${DateFormat("yyyy MM dd - HH:mm").format(dateTime)} $activityType$exerciseLegth perc';
  }

  List<String> choiseList = [
    "Nyugodt, nem megerőltető",
    "Kényelmes, kicsit megerőltető",
    "Megerőltető",
    "Nagyon megerőltető"
  ];
  String boxName = 'exercise';
  String titleText = 'Testmozgás';
  String callToActionText = 'Kövesd mennyit mozogsz.';
  List<Widget> get widgets {
    return [
      EntryElement.dateTimeInput("Aktivitás kezdete", (dateTime) {
        this.dateTime = dateTime;
      }, initialDateTime: this.dateTime),
      EntryElement.textInput('Aktivitás neve', hintText: 'Testmozgás típusa',
          onEventCallback: (text) {
        this.exerciseName = text;
      }, initialValue: this.exerciseName),
      EntryElement.numberInput('Aktivitás hossza',
          hintText: 'Testmozgás hossza',
          suffixText: '[perc]', onEventCallback: (number) {
        this.exerciseLegth = number;
      }, initialValue: this.exerciseLegth),
      EntryElement.titledMultipleChoiseInput(
          'Aktivitás megerőltetése', this.choiseList,
          onEventCallback: (String stringText) {
        this.exerciseDifficulty =
            choiseList.indexWhere((note) => note == stringText);
      },
          initialValue: (this.exerciseDifficulty == null)
              ? null
              : choiseList[this.exerciseDifficulty])
    ];
  }
}

@HiveType(typeId: 2)
class BloodPressure extends EntryModel {
  @HiveField(0)
  DateTime dateTime;

  @HiveField(1)
  int systolicPressure;

  @HiveField(2)
  int diastolicPressure;

  bool readyToSave() {
    return (this.dateTime != null &&
        this.systolicPressure != null &&
        this.diastolicPressure != null);
  }

  @override
  String shortDescription() {
    return '${DateFormat("yyyy MM dd - HH:mm").format(dateTime)} Vérnyomás: $systolicPressure/$diastolicPressure';
  }

  String boxName = 'bloodpressure';
  String titleText = 'Vérnyomás';
  String callToActionText = 'Jegyezd fel a vérnyomás értékeket.';
  List<Widget> get widgets {
    return [
      EntryElement.dateTimeInput("Aktivitás kezdete", (dateTime) {
        this.dateTime = dateTime;
      }, initialDateTime: this.dateTime),
      EntryElement.numberInput('Szisztolés vérnyomás',
          hintText: 'vérnyomás érték',
          suffixText: '[Hgmm]', onEventCallback: (number) {
        this.systolicPressure = number;
      }, initialValue: this.systolicPressure),
      EntryElement.numberInput('Diasztolés vérnyomás',
          hintText: 'vérnyomás érték',
          suffixText: '[Hgmm]', onEventCallback: (number) {
        this.diastolicPressure = number;
      }, initialValue: this.diastolicPressure),
    ];
  }
}

@HiveType(typeId: 3)
class SleepQualityGroningen extends EntryModel {
  @HiveField(0)
  DateTime dateTime;

  @HiveField(1)
  List answerList = List(15);

  List<String> questionList = [
    "Az éjjel mélyen aludtam.",
    "Úgy érzem, rosszul aludtam az éjjel.",
    "Több mint fél órába telt, amíg este el tudtam aludni.",
    "Többször is felébredtem az éjjel.",
    "Ma reggel fáradtan ébredtem.",
    "Úgy érzem, nem aludtam eleget az éjjel.",
    "Felébredtem az éjszaka közepén.",
    "Ma reggel kipihenten ébredtem.",
    "Azt hiszem, csak néhány órát aludtam az éjjel.",
    "Azt hiszem, jól aludtam az éjjel.",
    "Egy szemhunyásnyit sem aludtam az éjjel.",
    "Tegnap este könnyen elaludtam.",
    "Amikor éjjel felébredtem, nehezen aludtam vissza.",
    "Egész éjjel ide-oda forgolódtam az ágyban",
    "Az éjjel nem alhattam többet 5 óránál."
  ];

  List<String> choiseList = [
    "Igaz",
    "Nem Igaz",
  ];

  bool readyToSave() {
    for (var answer in answerList) {
      if (answer == null) return false;
    }
    return true;
  }

  int get resultValue {
    int result = 0;
    for (var i = 1; i < this.answerList.length; i++) {
      if (i == 7 || i == 9 || i == 11)
        result = result + answerList[i];
      else
        result = result - answerList[i] + 1;
    }
    return result;
  }

  @override
  String shortDescription() {
    return '${DateFormat("yyyy MM dd - HH:mm").format(dateTime)}: $resultValue';
  }

  String boxName = 'sleepqualitygroningen';
  String titleText = 'Alvásminőség (Groningen)';
  String callToActionText = 'Értékeld az előző éjszaka alvásminőségét.';
  List<Widget> get widgets {
    List<Widget> w = List<Widget>();
    w.add(EntryElement.dateTimeInput("Kitöltés Időpontja", (dateTime) {
      this.dateTime = dateTime;
    }, initialDateTime: this.dateTime));
    for (var i = 0; i < this.questionList.length; i++) {
      w.add(EntryElement.titledMultipleChoiseInput(
          this.questionList[i], this.choiseList,
          onEventCallback: (String stringText) {
        this.answerList[i] =
            choiseList.indexWhere((note) => note == stringText);
      },
          initialValue: (this.answerList[i] == null)
              ? null
              : this.choiseList[this.answerList[i]],
          oneAnswerPerLine: false));
    }
    return w;
  }
}
