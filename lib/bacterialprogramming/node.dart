import 'package:memo/bacterialprogramming/ability.dart';

class Node {
  Ability ability;
  Node parent;
  List<Node> children;
  var state;
  Node(this.ability, {this.parent, this.children, this.state}) {
    _initNode();
  }

  _initNode() {
    if (this.state == null &&
        !this.ability.isVariable &&
        this.ability.argTypesList == null) {
      state = Function.apply(ability.function, []);
    }
  }

  eval({Map variables}) {
    if (state != null) {
      return state;
    } else {
      if (ability.isVariable) {
        return variables[ability.variableName];
      } else {
        List args = List();
        if (this.children != null) {
          for (var child in children) {
            args.add(child.eval(variables: variables));
          }
        }
        return Function.apply(ability.function, args);
      }
    }
  }

  addChild(Node childNode) {
    if (children == null) {
      children = List<Node>();
    }
    children.add(childNode);
    childNode.parent = this;
    return childNode;
  }

  become(Node node, {takeOverChildren = false}) {
    if (node.hasChildren) {
      ability = node.ability;
      parent = node.parent;
      children = List.from(node.children);
      state = node.state;
      if (takeOverChildren) {
        for (Node child in this.children) {
          child.parent = this;
        }
      }
    } else {
      ability = node.ability;
      parent = node.parent;
      children = null;
      state = node.state;
    }
    _initNode();
  }

  clone() {
    if (this.hasChildren) {
      return Node(ability,
          parent: this.parent,
          children: List.from(this.children),
          state: this.state);
    } else {
      return Node(ability, parent: this.parent, state: this.state);
    }
  }

  bool get hasChildren {
    return (children != null);
  }

  bool get shouldHaveChildren {
    return (ability.argTypesList != null);
  }
}
