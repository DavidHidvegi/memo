class Ability {
  Function function;
  Type functionResultType;
  List<Type> argTypesList;
  String variableName;
  Ability(this.function, this.functionResultType, {this.argTypesList,
      this.variableName}) {
    validateSelf();
  }

  void validateSelf() {
    if (isVariable){
      if (argTypesList != null)
      print("Error: variable can not have args, argTypesList should be null");
    }
  }

  bool get isVariable {
    return (variableName != null);
  }
}
