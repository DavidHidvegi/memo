import 'dart:math';

import 'package:memo/bacterialprogramming/ability.dart';
import 'package:memo/bacterialprogramming/tree.dart';

class BacterialProgramming {
  List<Ability> abilities;
  Function fitnessFunction;
  int cloneCount;
  int infectionCount;

  int populationSize;
  List<Tree> population;
  int genCount;
  int limitNodeCount = 100;

  BacterialProgramming(this.abilities, this.fitnessFunction,
      {this.cloneCount = 5,
      this.infectionCount = 3,
      this.populationSize = 10,
      this.genCount = 0,
      this.population,
      this.limitNodeCount = 100}) {
    if (this.population == null) {
      this.population = List<Tree>();
      for (int i = 0; i < this.populationSize; i++) {
        this.population.add(Tree(this.abilities, limitNodeCount: this.limitNodeCount));
      }
    }
  }

  evolve() {
    this.bacterialMutate();
    this.geneTransfer();
    this.genCount = this.genCount + 1;
  }

  bacterialMutate() {
    for (Tree tree in this.population) {
      tree.bacterialMutate(this.fitnessFunction, this.cloneCount);
    }
  }

  geneTransfer() {
    for (int i = 0; i < this.infectionCount; i++) {
      this
          .population
          .sort((a, b) => fitnessFunction(a).compareTo(fitnessFunction(b)));
      int halfPopLen = this.population.length ~/ 2;
      List<Tree> superiorTrees = this.population.sublist(0, halfPopLen);
      List<Tree> inferiorTrees = this.population.sublist(halfPopLen);

      Random r = Random();
      superiorTrees[r.nextInt(superiorTrees.length)]
          .infectTree(inferiorTrees[r.nextInt(inferiorTrees.length)]);
    }
  }

  evolveUntil(int targetGenCount, double targetFitnessValue) {
    if (this.genCount >= targetGenCount) return;
    if (this.fitnessFunction(this.bestTree()) <= targetFitnessValue) return;
    this.evolve();
    this.evolveUntil(targetGenCount, targetFitnessValue);
  }

  Tree bestTree() {
    this
        .population
        .sort((a, b) => fitnessFunction(a).compareTo(fitnessFunction(b)));
    return population[0];
  }
}
