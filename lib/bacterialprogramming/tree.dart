import 'dart:math';

import 'package:memo/bacterialprogramming/ability.dart';
import 'package:memo/bacterialprogramming/node.dart';

class Tree {
  List<Ability> abilities;
  List<Ability> functionalAbilities;
  List<Ability> terminalAbilities;
  int limitNodeCount;
  Node root;

  Tree(this.abilities, {this.limitNodeCount = 200}) {
    functionalAbilities = List<Ability>();
    terminalAbilities = List<Ability>();
    for (Ability ability in this.abilities) {
      if (ability.argTypesList == null)
        terminalAbilities.add(ability);
      else
        functionalAbilities.add(ability);
    }
    Random random = Random();
    root = Node(this.abilities[random.nextInt(this.abilities.length)]);
    refillNodeWithChildrenRecursively(root);
  }

  int get nodeCount {
    return Tree.nodeCountFromNode(this.root);
  }

  List<Node> get listOfNodes {
    List<Node> nodes = List<Node>();

    List<Node> sumNodes(Node node, List<Node> nodeList) {
      nodeList.add(node);
      if (node.hasChildren) {
        for (Node child in node.children) {
          sumNodes(child, nodeList);
        }
      }
      return nodeList;
    }

    return sumNodes(this.root, nodes);
  }

  refillNodeWithChildrenRecursively(Node node) {
    if (!node.shouldHaveChildren) return;
    if (node.children == null)
      node.children = List();
    else
      node.children.clear();

    for (Type argType in node.ability.argTypesList) {
      node.addChild(createRandomChild(argType));
    }

    for (Node child in node.children) {
      if (child.ability.argTypesList != null) {
        refillNodeWithChildrenRecursively(child);
      }
    }
  }

  Node createRandomChild(Type childResultType) {
    List<Ability> possibleAbilities = List<Ability>();
    if (this.nodeCount > limitNodeCount) {
      for (Ability a in this.terminalAbilities) {
        if (a.functionResultType == childResultType) possibleAbilities.add(a);
      }
      if (possibleAbilities.length < 1) {
        for (Ability a in this.functionalAbilities) {
          if (a.functionResultType == childResultType) possibleAbilities.add(a);
        }
      }
    } else {
      for (Ability a in this.abilities) {
        if (a.functionResultType == childResultType) possibleAbilities.add(a);
      }
    }
    Random random = Random();
    return Node(possibleAbilities[random.nextInt(possibleAbilities.length)]);
  }

  static int nodeCountFromNode(Node node) {
    if (node.hasChildren) {
      int count = 1;
      for (Node child in node.children) {
        count = count + Tree.nodeCountFromNode(child);
      }
      return count;
    } else
      return 1;
  }

  eval({Map variables}) {
    return root.eval(variables: variables);
  }

  bacterialMutate(Function fitnessFunction, int cloneCount) {
    Random random = Random();
    List<Node> nodes = this.listOfNodes;
    _bacterialMutateAtNode(
        nodes[random.nextInt(nodes.length)], fitnessFunction, cloneCount);
  }

  _bacterialMutateAtNode(Node node, Function fitnessFunction, int cloneCount) {
    Node bestNode = node.clone();
    int bestFitness = Function.apply(fitnessFunction, [this]);

    List<Ability> possibleAbilities = List<Ability>();
    for (Ability a in this.abilities) {
      if (a.functionResultType == node.ability.functionResultType)
        possibleAbilities.add(a);
    }

    Random random = Random();
    for (var i = 0; i < cloneCount; i++) {
      Node newNode = Node(
          possibleAbilities[random.nextInt(possibleAbilities.length)],
          parent: node.parent);
      node.become(newNode);
      refillNodeWithChildrenRecursively(node);

      int newFittness = Function.apply(fitnessFunction, [this]);
      if (newFittness < bestFitness) {
        bestFitness = newFittness;
        bestNode = node.clone();
      }
    }

    node.become(bestNode, takeOverChildren: true);
  }

  infectTree(Tree targetTree) {
    List<Node> nodes = this.listOfNodes;
    Random r = Random();
    Node baseRoot = nodes[r.nextInt(nodes.length)];

    List<Node> possibleTargetNodes = List<Node>();
    for (Node node in targetTree.listOfNodes) {
      if (node.ability.functionResultType ==
          baseRoot.ability.functionResultType) possibleTargetNodes.add(node);
    }

    if (possibleTargetNodes.length > 0) {
      Node targetRoot =
          possibleTargetNodes[r.nextInt(possibleTargetNodes.length)];
      _copySubtree(baseRoot, targetRoot);
    }
  }

  _copySubtree(Node baseNodeRoot, Node targetNodeRoot) {
    void cloneArgsRecursively(Node rootNode) {
      if (rootNode.ability.argTypesList == null) return;
      List<Node> children = List.from(rootNode.children);
      rootNode.children.clear();
      for (Node child in children) {
        Node newChild = rootNode.addChild(child.clone());
        cloneArgsRecursively(newChild);
      }
    }

    Node targetParent = targetNodeRoot.parent;
    targetNodeRoot.become(baseNodeRoot);
    targetNodeRoot.parent = targetParent;
    cloneArgsRecursively(targetNodeRoot);
  }
}
