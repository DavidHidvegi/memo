# Welcome to Memo!

Hi! Memo is my BSc thesis project. It is an open source Flutter app that you can use to collect data about the things that affect you in order to discover more about yourself. 

Find the Bacterial Programming Python package [here](https://bit.ly/bmebact) and the Thesis in hungarian [here](https://www.overleaf.com/read/gxmmtpkgwczy).

> Written with [StackEdit](https://stackedit.io/).